package com.badwords.dao;

import org.springframework.security.core.GrantedAuthority;

/*
 * Implementation of GrantedAuthority. Defines access level of logged user
 */
public enum Role implements GrantedAuthority {
	user("user"), 
	admin("admin");

	private final String authority;
	
    Role(String authority) {
		this.authority = authority;
	}
    
	@Override
	public String getAuthority() {
		return authority;
	}

}
