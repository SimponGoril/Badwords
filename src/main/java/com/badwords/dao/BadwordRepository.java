package com.badwords.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BadwordRepository extends JpaRepository<Badword, Long>{
	
	/**
	 * check if badword is already in database
	 * @param badword to be checked
	 * @return true if badword already exists in database
	 */
	boolean existsByWord(String badword);
	
	@Query("SELECT word FROM badword")
	List<String> getBadwordsAsString();
}
