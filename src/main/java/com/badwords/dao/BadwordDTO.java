package com.badwords.dao;

public class BadwordDTO {

	private long id;
	
	private String word;
	
	public BadwordDTO() {}
	
	public BadwordDTO(Badword model) {
		this.id = model.getId();
		this.word = model.getWord();
	}
	
	public BadwordDTO(long id, String word) {
		this.id = id;
		this.word = word;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	
}
