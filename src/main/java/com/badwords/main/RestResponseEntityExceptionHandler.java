package com.badwords.main;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class that cover exception handling for frontend users
 * @author Sismon
 *
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = Logger.getLogger(RestResponseEntityExceptionHandler.class);
	
    @ExceptionHandler(value = { IllegalArgumentException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) throws JsonProcessingException {
    	
    	logger.debug("Unable to handle request. Reason: " + ex.getMessage());
    	
        return handleExceptionInternal(ex, new ObjectMapper().writeValueAsString(ex.getMessage()), 
          new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
