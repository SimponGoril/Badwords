package com.badwords.controllers;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.badwords.dao.User;
import com.badwords.services.UserDetailsServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class UserController {
	
	@Autowired
	private UserDetailsServiceImpl userService;

	/**
	 * Login method that uses JWT. 
	 * @param login - user object
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<String> login(@RequestBody User login) throws ServletException, JsonProcessingException {
		return userService.login(login);
	}
	
	/**
	 * Creates user account 
	 * @param user - user object
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseEntity<String> createUser(@RequestBody User user) throws ServletException, JsonProcessingException {
		return userService.createUser(user);
	}
}
