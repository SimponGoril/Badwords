package com.badwords.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.badwords.dao.BadwordDTO;
import com.badwords.services.BadwordService;

@Controller
@RequestMapping("/*")
public class StaticController {

	@Autowired
	private BadwordService bs;
	
	/*
	 * Fallback controller for serving FE
	 */
	@RequestMapping(value = "*", method = RequestMethod.GET) 
	public ModelAndView fallback() {
		ModelAndView model = new ModelAndView("index");
		return model;
	}
}
