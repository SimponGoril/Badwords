package com.badwords.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.badwords.dao.BadwordDTO;
import com.badwords.services.BadwordService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/api/*")
public class BadwordController {
	
	@Autowired
	private BadwordService badwordService;

	/**
	 * Retrieves badword from database. Requires admin access rights
	 * @param id - badword id
	 * @return 200, 404, 403 status response
	 */
	@RequestMapping(value = "/badword/{id}", method = RequestMethod.GET)
	public ResponseEntity<String> getBadword(@PathVariable long id) throws JsonProcessingException {
		return badwordService.getOne(id);
	}

	/**
	 * Deletes badword from database. Requires admin access rights
	 * @param id - badword id
	 * @return 200, 404, 403 status response
	 */
	@RequestMapping(value = "/badword/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteBadword(@PathVariable long id) throws JsonProcessingException {
		return badwordService.deleteOne(id);
	}
	
	/**
	 * Updates badword in database. Requires admin access rights
	 * @param id - badword id
	 * @return 200, 404, 403 status response
	 */
	@RequestMapping(value = "/badword", method = RequestMethod.PUT)
	public ResponseEntity<String> putBadword(@RequestBody BadwordDTO dto) {
		return badwordService.updateOne(dto);
	}
	
	/**
	 * creates new badword in database. Requires admin access rights
	 * @param id - badword string
	 * @return 200, 404, 403, 409 status response
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/badword", method = RequestMethod.POST)
	public ResponseEntity<String> postBadword(@RequestParam(value = "badword") String badword) throws JsonProcessingException {
		return badwordService.save(badword);
	}

	/**
	 * Get list of badwords that fit specified character limit
	 * @param characterLimit - Character limit for paragraph of badwords
	 * @return 200, 404, 403 status response
	 */
	@RequestMapping(value = "/badword", method = RequestMethod.GET)
	public ResponseEntity<String> getBadwords(@RequestParam(value = "characterLimit") int characterLimit) throws JsonProcessingException {		
		return badwordService.getAnyAsString(characterLimit);
	}

	/**
	 * creates new badwords from csv file. in case of error, it skip saving and returns map of wrong badwords to client
	 * @param file csv file 
	 * @return 200, 400, 403
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/badword/csv", method = RequestMethod.POST)
	public ResponseEntity<String> postBadwordCsv(@RequestParam("file") MultipartFile file) throws JsonProcessingException {
		return badwordService.save(file);
	}
	
	/**
	 * prepare and return csv with all badwords
	 * @returns all badwords as csv file
	 * @throws JsonProcessingException, IOException
	 */
	@RequestMapping(value = "/badword/csv", method = RequestMethod.GET)
	public ResponseEntity<Resource> getBadwordCsv() throws IOException {
		return badwordService.getBadwordCsv();
	} 
	
	/**
	 * Returns random badword from database as DTO object
	 * @return 200, 403 status response
	 */
	@RequestMapping(value = "/badword/random", method = RequestMethod.GET)
	public ResponseEntity<String> getAny() throws JsonProcessingException {
		return badwordService.getAny();
	}
}
