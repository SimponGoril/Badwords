package com.badwords.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.badwords.dao.Badword;
import com.badwords.dao.BadwordDTO;
import com.badwords.dao.BadwordRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Service class for CRUD of Badword objects with additional utility classes
 * @author Sismon
 *
 */
@Service
public class BadwordService {

	@Autowired
	private BadwordRepository br;
	
	private static final Logger logger = Logger.getLogger(BadwordService.class);
	
	private Random rand = new Random();
	
	/**
	 *  returns one badword as dto object
	 *  @param id - id of badword object to be retrieved
	 *  @throws JsonProcessingException 
	 */
	public ResponseEntity<String> getOne(Long id) throws JsonProcessingException {
		
		// check if badword is present
		if(br.exists(id)) {
			logger.debug("retriving badword id " + id);
			
			// prepare DTO object
			final BadwordDTO result = new BadwordDTO(br.findOne(id));
			
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString(result), HttpStatus.OK);
		}
		throw new IllegalArgumentException("Badword id " + id + " not found");
	}
	
	/**
	 *  saves one badword to database
	 *  @param badword - string of new badword object to be saved
	 */
	@Transactional
	public ResponseEntity<String> save(String badword) throws JsonProcessingException {
		
		// check if badword is valid
		checkBadword(badword);
			
		// check if it is already in database
		if(!br.existsByWord(badword)) {
			logger.debug("saving badword:" + badword);
			
			Badword newBadword = new Badword(badword); 
			br.save(newBadword);
			
			return new ResponseEntity<>(HttpStatus.CREATED);
		}
		throw new IllegalArgumentException("Badword " + badword + " i already in database");
	}
	
	/**
	 *  saves badwords from csv file to database
	 *  @param badword - string of new badword object to be saved
	 */
	@Transactional
	public ResponseEntity<String> save(MultipartFile file) throws JsonProcessingException {
		
		// lets open input stream from file and start parsing
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
			
			// prepare array of wrong badwords
			Map<String, String> badBadwords = new HashMap<>(); 
			List<String> goodBadwords = new ArrayList<>(); 
			String line = null;
			
			while((line = reader.readLine()) != null) {
				
				// check badword. if it is wrong, add it to list of bad badwords, else prepare it to save
				if(line.length() >= 100 || line.length() <= 0) {
					badBadwords.put(line, "Length is not between <1 - 100>");
				} else if (line.contains(" ")) {
					badBadwords.put(line, "Badword contains space");
				} else if (br.existsByWord(line)) {
					badBadwords.put(line, "Badword already in database");
				} else {
					goodBadwords.add(line);
				}
			}
			
			// if there are errors, throw exception, else proceed to save them
			if(badBadwords.size() > 0) {
				throw new IllegalArgumentException("Unable to save badwords from csv. Bad badwords:" + badBadwords.toString());
			} else {
				for(String goodBadword : goodBadwords) {
					save(goodBadword);
				}
			}
			
		} catch (IOException e) {
			throw new IllegalArgumentException("Unable to read file. Reason: " + e.getMessage());
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * Prepare and return csv file with badword
	 * @return csv file with badword
	 * @throws Exception 
	 */
	public ResponseEntity<Resource> getBadwordCsv() throws IOException {
		
		// prepare output stream
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			
			// retrieve only words from database and add them to stream
			List<String> badwords = br.getBadwordsAsString();
			for(String badword : badwords) {
				badword = badword.concat("\r\n");
				baos.write(badword.getBytes());
			}

			// return response
		    return ResponseEntity
		            .ok()
		            .header("Content-Disposition","attachment; filename=\"sbadwords.csv\"")
		            .contentType(MediaType.TEXT_PLAIN)
		            .body(new InputStreamResource(new ByteArrayInputStream (baos.toByteArray())));
		} catch (IOException exception) {
			logger.error("Unable to prepare badword csv. Reason:" + exception);
			throw exception;
		}
		
	}
	
	/**
	 *  deletes badword
	 *  @param id - id of badword object to be retrieved
	 */
	public ResponseEntity<String> deleteOne(Long id) {
		
		// check if badword is present
		if(br.exists(id)) {
			logger.debug("deleting badword id " + id);
			
			br.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		throw new IllegalArgumentException("Badword with id " + id + " not found");
	}
	
	/**
	 *  updates badword
	 *  @param dto - simplified dto object to be updated
	 */
	public ResponseEntity<String> updateOne(BadwordDTO dto) {
		
		// check if badword is present
		if(br.exists(dto.getId())) {
			logger.debug("updating badword id " + dto.getId());
			
			// prepare DTO object
			final Badword badword = br.findOne(dto.getId());
			
			// validate put request
			final String newBadword = dto.getWord();
			checkBadword(newBadword);
			
			// update badword in database
			badword.setWord(dto.getWord());
			badword.setModified(Calendar.getInstance().getTime());
			br.save(badword);
			return new ResponseEntity<>(HttpStatus.OK);

		}
		throw new IllegalArgumentException("Badword with id " + dto.getId() + " not found");
	}
	
	/**
	 * private method to check quality of badword. Proper badword should have 0 - 100 chars without space
	 * @param badword to be checked
	 */
	private void checkBadword(String badword) {
		
		// lets check basic string length
		if(badword.length() == 0 || badword.length() > 100) {
			throw new IllegalArgumentException("Badword length is not between 0 - 100 char");
		}
		
		// and spaces in badword
		if(badword.contains(" ")) {
			throw new IllegalArgumentException("Badword contains space");
		}
	}
	
	/**
	 * @returns random badword as dto
	 * @throws JsonProcessingException 
	 */
	public ResponseEntity<String> getAny() throws JsonProcessingException {
		final Long randomBadWordId = getRandomBadwordId();
		final BadwordDTO result = new BadwordDTO(br.findOne(randomBadWordId));
		return new ResponseEntity<>(new ObjectMapper().writeValueAsString(result), HttpStatus.OK);
	}
	
	/**
	 * returns random set of badwords that fits param characterLimit
	 * @param characterLimit - character limit of badword paragraph
	 * @throws JsonProcessingException 
	 */
	public ResponseEntity<String> getAnyAsString(int characterLimit) throws JsonProcessingException {
		
		// validate character limit
		if(characterLimit <= 0) {
			throw new IllegalArgumentException("Invalid character limit");
		}
		
		final StringBuffer result = new StringBuffer();
		
		while(result.length() < characterLimit) {
			Long randomBadWord = getRandomBadwordId();
			BadwordDTO randomBadword = new BadwordDTO(br.findOne(randomBadWord));
			result.append(randomBadword.getWord() + " ");
		}		
		
		// trim oveflowing characters and return response
		return new ResponseEntity<>(new ObjectMapper().writeValueAsString(result.substring(0, characterLimit)),HttpStatus.OK);
	}
	
	/**
	 *  utility method for retrival of set of badword dto
	 *  @param number - specifies number of badwords to be returned
	 *  @returns random set of badword dto objects
	 */
	public Set<BadwordDTO> getAny(int number) {
		HashSet<BadwordDTO> result = new HashSet<>();
		
		for(int i = 0; i < number; i++) {
			Long randomBadWord = getRandomBadwordId();
			BadwordDTO randomBadword = new BadwordDTO(br.findOne(randomBadWord));
			result.add(randomBadword);
		}
		
		return result;
	}
	
	/**
	 * counts database results and picks random badword id for further use in random functions
	 * @return random long <1 - n>
	 */
	private Long getRandomBadwordId() {
		final long randomBadWordId = (long)(rand.nextDouble()*br.count() + 1);
		return randomBadWordId;
	}
}
