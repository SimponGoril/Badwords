package com.badwords.services;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.badwords.dao.Role;
import com.badwords.dao.User;
import com.badwords.dao.UserPrincipal;
import com.badwords.dao.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    
    private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    
    private static final Logger logger = Logger.getLogger(UserDetailsServiceImpl.class);
 
    @Override
    public UserPrincipal loadUserByUsername(String username) {
    	
    	logger.debug("Getting user:" + username);
    	
        User user = userRepository.findByUsername(username);
        if (user == null) {
        	logger.debug("user:" + username + " was not found in database");
            throw new UsernameNotFoundException(username);
        }
        return new UserPrincipal(user);
    }
    
    /**
     * Login method
     * @param login
     * @return 200, 400, 404
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> login(User login) throws JsonProcessingException {
	    
	    if (login.getUsername() == null || login.getPassword() == null) {
	    	logger.debug("Unable to autenthicate user. Reason: No user credientals added");
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    }

	    try {
		    // prepare local variables
	    	final String username = login.getUsername();
	    	UserPrincipal user = loadUserByUsername(username);
	    	String jwtToken = "";
		    final String pwd = user.getPassword();
		    final String password = login.getPassword();
		    
		    if (!encoder.matches(password, pwd)) {
		    	logger.debug("Unable to autenthicate user:" + login.getUsername() + " Reason: wrong password");
		    	throw new IllegalArgumentException("Invalid password");
		    }

		    // if the password is ok, generate token for further requests
		    jwtToken = Jwts.builder().setSubject(username).claim("roles", user.getAuthorities()).setIssuedAt(new Date())
		            .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

		    return new ResponseEntity<>(new ObjectMapper().writeValueAsString(jwtToken), HttpStatus.OK);
	    } catch (Exception e) {
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    }

    }
    
    /**
     * Create user method
     * @param newUser
     * @return 200, 400
     */
	public ResponseEntity<String> createUser(User newUser) {
		
		// lets valid new username
		if(newUser.getUsername() == null || newUser.getUsername().length() > 50) {
			throw new IllegalArgumentException("Username is null or longer than 50 characters");
		}
		
		// check if username is already taken
		if(userRepository.existsByUsername(newUser.getUsername())) {
			throw new IllegalArgumentException("Username is taken");
		}
		
		// lets valid password
		if(newUser.getPassword() == null || newUser.getPassword().length() < 4) {
			throw new IllegalArgumentException("Password is null or smaller than 4 characters");
		}
		
		logger.debug("Creating new user " + newUser.getUsername());
		
		// encrypt password
		final String encodedPassword = encoder.encode(newUser.getPassword());
		
		// prepare new entity object
		User user = new User(newUser.getUsername(), encodedPassword);
		user.setRole(Role.user);
		
		// save user and return ok
		userRepository.save(user);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	

}
