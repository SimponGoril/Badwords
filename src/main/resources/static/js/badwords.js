/**
 * 
 */

angular.module('badwords', [])
.controller('controller', function($scope, $http) {
	
	$scope.badword = "";
	$scope.count = 50;
	$scope.getBadword = function(count) {
	    $http.get('http://localhost:8080/Badwords/api/', {params: {characterLimit: count}}).
	        then(function(response) {
	            $scope.badword = response.data;
	        });
	}
});